# PB Server (Project Board Server)

PB Server is the backend for what will be the new version of [Project Board](https://github.com/CrowderSoup/Project-Board).
Project Board started as a learning project, and it will continue as such. When 
I first built it I found it a useful tool for my own purposes, as did a few of 
my friends. The goal (in addition to learning something new) is that I can build
something that will be more generally useful to a wider set of people.

I am developing this project as open source, under the MIT License. I will update
this readme with instructions for developers on setting up their own instance of 
Project Board Server, as well as a guide for contributing, as those things become
available.